# config

# Post Installation

Pacman modification to my terms.
## For parallel downloads
Edit file:
```
sudo nano /etc/pacman.conf
```
Uncomment line for [[ parallel download ]]
Save & exit

## Much needed packages
```
sudo pacman -S code telegram-desktop flameshot vlc libreoffice-still pipewire simplescreenrecorder --noconfirm
```

## List orphaned packages(Non Used Packages)
```
pacman -Qqd | pacman -Rsu --print -
```
### Now Removing them
```
pacman -Qtdq | pacman -Rns -
```
**The -Qt only truly lists true orphans 
## Fonts Installation
```
git clone https://gitlab.com/aryalawjju23/config.git
```
### Now install the fonts from txt file <permission needed>
```
pacman -S --needed - < config/fonts/fonts.txt

```
